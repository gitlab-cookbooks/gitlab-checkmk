name             'gitlab-checkmk'
maintainer       'GitLab Inc.'
maintainer_email 'jeroen@gitlab.com'
license          'MIT'
description      'Installs/Configures gitlab-checkmk'
long_description IO.read(File.join(File.dirname(__FILE__), 'README.md'))
version          '0.1.18'

depends 'gitlab-vault'
