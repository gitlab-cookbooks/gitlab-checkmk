# gitlab-checkmk CHANGELOG

This file is used to list changes made in each version of the gitlab-checkmk cookbook.

## 0.1.16
- Jeroen - Improved redis_replication_lag check for redis sentinel

## 0.1.15
- Jeroen - Added missing proxy-port.conf

## 0.1.14
- Jeroen - Upgrade check_mk to 1.2.8p13

## 0.1.13
- Jeroen - Use gitlab-vault and update license

## 0.1.0
- Jeroen - Initial release of gitlab-checkmk
