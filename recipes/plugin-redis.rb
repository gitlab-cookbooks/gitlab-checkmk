#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-redis
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
include_recipe 'gitlab-vault'
plugin_redis_conf = GitLab::Vault.get(node, 'gitlab-checkmk', 'plugin-redis')

template '/usr/lib/check_mk_agent/local/redis_keys.sh' do
  mode '0700'
  owner 'root'
  group 'root'
  variables(
    password: plugin_redis_conf['password']
  )
end

template '/usr/lib/check_mk_agent/local/redis_replication_lag.sh' do
  mode '0700'
  owner 'root'
  group 'root'
  variables(
    password: plugin_redis_conf['password']
  )
end
