#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-logwatch
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
plugin_dir = '/usr/lib/check_mk_agent/plugins'

cookbook_file "#{plugin_dir}/mk_unicorn_worker_age.sh" do
  mode '0755'
  owner 'root'
  group 'root'
end
