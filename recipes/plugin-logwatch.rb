#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-logwatch
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
plugin_dir = '/usr/lib/check_mk_agent/plugins'
plugin_conf_dir = '/etc/check_mk'
logwatch_conf = 'logwatch.cfg'

if node['platform_family'] == 'rhel'
  logwatch_conf = 'logwatch.rhel.cfg'
end

cookbook_file "#{plugin_dir}/mk_logwatch" do
  mode '0755'
  owner 'root'
  group 'root'
end

directory plugin_conf_dir do
  mode '0775'
  owner 'root'
  group 'root'
end

cookbook_file "#{plugin_conf_dir}/logwatch.cfg" do
  source logwatch_conf
  mode '0755'
  owner 'root'
  group 'root'
end
