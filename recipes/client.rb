#
# Cookbook Name:: gitlab-checkmk
# Recipe:: client
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
package 'xinetd'

service 'xinetd' do
  supports restart: true
  action :enable
end

if node['platform_family'] == 'debian'
  cookbook_file '/var/tmp/check-mk-agent_1.2.6p16-1_all.deb'

  bash 'install checkmk agent' do
    code <<-EOH
      dpkg -i /var/tmp/check-mk-agent_1.2.6p16-1_all.deb
    EOH
    not_if "dpkg -l|grep -q 'check-mk-agent'"
  end
end

if node['platform_family'] == 'rhel'
  cookbook_file '/var/tmp/check-mk-agent-1.2.6p16-1.noarch.rpm'

  bash 'install checkmk agent' do
    code <<-EOH
      rpm -i /var/tmp/check-mk-agent-1.2.6p16-1.noarch.rpm
    EOH
    not_if "rpm -qa|grep -q 'check-mk-agent'"
  end
end

server = node['gitlab-checkmk']['omd']['server']
template '/etc/xinetd.d/check_mk' do
  mode '0644'
  variables(
    server: server
  )
  notifies :restart, 'service[xinetd]'
end
