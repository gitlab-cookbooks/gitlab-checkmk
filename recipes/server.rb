#
# Cookbook Name:: gitlab-checkmk
# Recipe:: server
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
include_recipe 'gitlab-vault'
omd_conf = GitLab::Vault.get(node, 'gitlab-checkmk', 'omd')

%w(curl rpm lcab libgsf-1-114 python-reportlab python-imaging poppler-utils).each {|pkg| package pkg}

bash 'install checkmk raw' do
  code <<-EOH
    curl -s https://mathias-kettner.de/support/1.2.8p13/check-mk-raw-1.2.8p13_0.xenial_amd64.deb > /tmp/check-mk-raw-1.2.8p13_0.xenial_amd64.deb
    dpkg -i /tmp/check-mk-raw-1.2.8p13_0.xenial_amd64.deb
  EOH
  not_if "dpkg -l|grep -q 'check-mk-raw'"
end

site = node['gitlab-checkmk']['omd']['site']
bash 'setup omd' do
  code <<-EOH
    omd create #{site}
    omd config #{site} set DEFAULT_GUI check_mk
    omd restart
  EOH
  creates "/opt/omd/sites/#{site}"
end

# Setup oauth2
template "/opt/omd/sites/#{site}/etc/apache/proxy-port.conf" do
  mode '0640'
  owner site
  group site
  variables(
    site: site
  )
  notifies :run, 'bash[restart omd]'
end

# Setup https
template "/opt/omd/sites/#{site}/etc/apache/conf.d/ssl.conf" do
  mode '0640'
  owner site
  group site
  variables(
    site: site
  )
  notifies :run, 'bash[restart omd]'
end
directory "/opt/omd/sites/#{site}/etc/apache/ssl" do
  owner site
  group site
  mode '0755'
  action :create
end
file "/opt/omd/sites/#{site}/etc/apache/ssl/#{site}.pem" do
  content omd_conf['ssl_cert']
  mode '0644'
  owner site
  group site
  notifies :run, 'bash[restart omd]'
end
file "/opt/omd/sites/#{site}/etc/apache/ssl/#{site}.key" do
  content omd_conf['ssl_cert_key']
  mode '0640'
  owner site
  group site
  notifies :run, 'bash[restart omd]'
end
file "/opt/omd/sites/#{site}/etc/apache/ssl/#{site}-chain.pem" do
  content omd_conf['ssl_cert_chain']
  mode '0644'
  owner site
  group site
  notifies :run, 'bash[restart omd]'
end

bash 'restart omd' do
  code <<-EOH
    omd restart
  EOH
  action :nothing
end

link '/etc/apache2/sites-enabled/omd-ssl.conf' do
  to "/opt/omd/sites/#{site}/etc/apache/conf.d/ssl.conf"
  notifies :restart, 'service[apache2]'
end

service 'apache2' do
  action :enable
end

# Add slack notification method
template "/opt/omd/sites/#{site}/share/check_mk/notifications/slack.sh" do
  mode '0755'
  owner 'root'
  group 'root'
  variables omd_conf.to_hash
end

# Add pagerduty notification method
template "/opt/omd/sites/#{site}/share/check_mk/notifications/pagerduty.sh" do
  mode '0755'
  owner 'root'
  group 'root'
  variables omd_conf.to_hash
end

# Add notifications check
template "/usr/lib/check_mk_agent/local/mk_notifications.sh" do
  mode '0755'
  owner 'root'
  group 'root'
  variables omd_conf.to_hash
end

# Fetch all checkmk agents for main.mk
clients = search(:node, 'recipes:gitlab-checkmk\:\:client').map do |n|
  # Use localhost for checkmk server
  if n.name == node['fqdn']
    [node['fqdn'], '127.0.0.1']
  else
    [n.name, n.ipaddress]
  end
end.compact

clients.sort_by!(&:first) unless clients.empty?

template "/opt/omd/sites/#{site}/etc/check_mk/main.mk" do
  mode '0644'
  variables(
    clients: clients
  )
  notifies :run, 'bash[reload checkmk configuration]'
end

template "/opt/omd/sites/#{site}/etc/check_mk/multisite.mk" do
  mode '0644'
  notifies :run, 'bash[reload checkmk configuration]'
end

bash 'reload checkmk configuration' do
  code <<-EOH
    sudo -i -u #{site} cmk -II
    sudo -i -u #{site} cmk -O
  EOH
  action :nothing
end
