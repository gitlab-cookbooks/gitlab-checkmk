#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-apt_download
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
cookbook_file '/usr/lib/check_mk_agent/local/apt-download.sh' do
  mode '0755'
  owner 'root'
  group 'root'
end

package = node['gitlab-checkmk']['plugin-apt_download']['package']
cron 'checkmk_apt_download' do
  minute node['gitlab-checkmk']['plugin-apt_download']['cron_minute']
  command "/usr/lib/check_mk_agent/local/apt-download.sh cron #{package}"
end
