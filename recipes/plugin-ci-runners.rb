#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-nfsiostat
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
package 'sysstat'

local_plugin_dir = "/usr/lib/check_mk_agent/local"

directory local_plugin_dir do
  mode '0755'
  owner 'root'
  group 'root'
  recursive true
end

cookbook_file "#{local_plugin_dir}/mk_ci_runners" do
  mode '0755'
  owner 'root'
  group 'root'
end
