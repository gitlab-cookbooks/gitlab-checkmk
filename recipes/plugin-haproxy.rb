#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-haproxy
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#

directory '/usr/lib/check_mk_agent/local' do
  recursive true
end

cookbook_file '/usr/lib/check_mk_agent/local/haproxychecks.py' do
  mode '0644'
  owner 'root'
  group 'root'
end
cookbook_file '/usr/lib/check_mk_agent/local/haproxy.py' do
  mode '0755'
  owner 'root'
  group 'root'
end
