#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-postgres
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
cookbook_file '/usr/lib/check_mk_agent/plugins/mk_postgres' do
  mode '0755'
  owner 'root'
  group 'root'
end
