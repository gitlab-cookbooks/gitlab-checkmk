#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-postgres
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#

plugin_dir = '/usr/lib/check_mk_agent/plugins'

directory plugin_dir do
  mode '0755'
  owner 'root'
  group 'root'
  recursive true
end

cookbook_file "#{plugin_dir}/mk_postgres" do
  mode '0755'
  owner 'root'
  group 'root'
end

cookbook_file "#{plugin_dir}/postgres_locks" do
  mode '0755'
  owner 'root'
  group 'root'
end

cookbook_file "#{plugin_dir}/postgres_connections" do
  mode '0755'
  owner 'root'
  group 'root'
end

cookbook_file "#{plugin_dir}/postgres_sessions" do
  mode '0755'
  owner 'root'
  group 'root'
end

cookbook_file "#{plugin_dir}/postgres_query_duration" do
  mode '0755'
  owner 'root'
  group 'root'
end

cookbook_file "#{plugin_dir}/postgres_bloat" do
  mode '0755'
  owner 'root'
  group 'root'
end
