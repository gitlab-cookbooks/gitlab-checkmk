#
# Cookbook Name:: gitlab-checkmk
# Recipe:: plugin-rsyslog
# License:: MIT
#
# Copyright 2016, GitLab Inc.
#
cookbook_file '/usr/lib/check_mk_agent/local/mk_rsyslog.sh' do
  mode '0755'
  owner 'root'
  group 'root'
end
