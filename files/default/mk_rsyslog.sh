#!/bin/bash
CDATE=`date +%Y%m%d%H`
HDATE=`date --date="1 hour ago" +%Y%m%d%H`
if [ -f /gitlab-log/104.208.241.215/104.208.241.215-${CDATE} ]; then
  echo "0 Rsyslog - OK - rsyslog updates logs in /gitlab-log/104.208.241.215"
  exit
fi
if [ -f /gitlab-log/104.208.241.215/104.208.241.215-${HDATE} ]; then
  echo "0 Rsyslog - OK - rsyslog updates logs in /gitlab-log/104.208.241.215"
  exit
fi
echo "1 Rsyslog - WARNING - rsyslog doesn't update logs in /gitlab-log/104.208.241.215, please check!"
