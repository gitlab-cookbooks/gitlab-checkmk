#!/bin/bash
USER=gitlab-psql
WARNING=500

SLAVE=`ps -ef|grep -c "wal\ receiver"`
if [ "$SLAVE" -ne "0" ]; then
  echo "0 PostgreSQL_replication_lag pg_repl_lag=0;$WARNING;;0; OK - wal receiver detected: this check is for master only"
  exit
fi

PG_STAT="$(echo "select client_addr,pg_xlog_location_diff(pg_current_xlog_insert_location(), flush_location) from pg_stat_replication order by client_addr desc;" |\
    su - $USER -c "psql -h /var/opt/gitlab/postgresql --variable ON_ERROR_STOP=1 -d postgres -A -t -F':'" 2>/dev/null)"

for ENTRY in $PG_STAT
do
  NODE=`echo $ENTRY|awk -F: '{ print $1 }'`
  LAG=`echo $ENTRY|awk -F: '{ print $2 }'`
  LAG_KBYTES=`expr $LAG / 1024`

  if [ "$LAG_KBYTES" = "" ]; then
    echo "2 PostgreSQL_replication_lag pg_repl_lag=0;500;;0; CRITICAL - replication stopped"
    exit
  fi
  if [ "$LAG_KBYTES" -gt "$WARNING" ]; then
    echo "1 PostgreSQL_replication_lag pg_repl_lag=$LAG_KBYTES;$WARNING;;0; WARNING - replication lag to high: $LAG_KBYTES KB"
    exit
  fi
done

if [ "$PG_STAT" = "" ]; then
  echo "2 PostgreSQL_replication_lag pg_repl_lag=0;500;;0; CRITICAL - replication stopped"
  exit
fi
echo "0 PostgreSQL_replication_lag pg_repl_lag=$LAG_KBYTES;$WARNING;;0; OK - replication lag: $NODE $LAG_KBYTES KB"
