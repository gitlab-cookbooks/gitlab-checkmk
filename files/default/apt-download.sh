#!/bin/sh
cron () {
  cd /tmp

  /usr/bin/apt-get download $1 >/dev/null 2>&1
  STATUS=$?

  MSG="$STATUS apt-download - OK - $1 package download successful"

  /bin/rm -f /tmp/$1*.deb*

  if [ $STATUS -ne 0 ]; then
    STATUS=1
    MSG="$STATUS apt-download - WARNING - $1 package download failed!"
  fi

  echo $MSG > /tmp/apt-download.log
}

if [ "$1" = "cron" ];then
  cron $2
else
  cat /tmp/apt-download.log
fi
exit 0
