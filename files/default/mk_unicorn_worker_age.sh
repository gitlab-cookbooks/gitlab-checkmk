#!/bin/sh

# Variables m1, .., m5 contain date strings pointing to 1, .. 5 minutes ago
awk $(for m in $(seq 5); do echo "-vm$m=$(date +'%Y-%m-%dT%H:%M' --date="$m minute ago")"; done) '
($2 ~ m1 || $2 ~ m2  || $2 ~ m3 || $2 ~ m4 || $2 ~ m5) && /Unicorn::WorkerKiller.*alive:/ {
  age[count] = $(NF - 3)
  total += age[count]
  count += 1
}
END {
  print "<<<local>>>"
  if (count == 0) {
    print "0 UNICORN_WORKER_AGE average.value=0;;;0;|pct90.value=0;;;0; OK"
    exit 0
  }

  asort(age)
  print "0 UNICORN_WORKER_AGE average.value=" total/count ";;;0;|pct90.value=" age[int(0.9*count)] ";;;0; OK"
}
' /var/log/gitlab/unicorn/unicorn_stderr.log
