#!/bin/bash
tmp_file=/usr/lib/check_mk_agent/local/nfsstats.tmp

retrans_file=0
if [ -f $tmp_file ]; then
    retrans_file=$(cat $tmp_file)
fi
retrans_cur=$(nfsstat -rc|head -3|tail -1|awk '{ print $2}')
echo $retrans_cur > $tmp_file

retrans=$(expr $retrans_cur - $retrans_file)
if [ $retrans -lt 1 ]; then
    status=0
    statustxt=OK
elif [ $retrans -lt 5 ]; then
    status=1
    statustxt=WARNING
else
    status=2
    statustxt=CRITICAL
fi
echo "$status NFS_retrans retrans=$retrans;1;5;0; $statustxt - NFS retrans counts $retrans"
