# Example checks for the HAProxy plugin
checks = [
    #fieldname, warning, critical
    ['scur', '20000', '20000'],
    ['status', '', '']
]

if __name__ == "__main__":
    print "This file is not meant to be called directly"
