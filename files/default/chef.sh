#!/bin/sh
MSG="0 Chef - OK - all nodes are healthy"

STATUS=$(knife status -c /etc/chef/client.rb -u chef.gitlab.com -H --no-color 2>/dev/null | grep -v "1 hour  ago" | head -1)
if [ "$STATUS" != "" ]; then
  MSG="1 Chef - WARNING - $STATUS"
fi

echo $MSG
exit 0
