#!/bin/bash

sudo gitlab-rails runner '
WARNING = 750
CRITICAL = 1000
CRITICAL_QUEUE = 1000

stats = Sidekiq::Stats.new

queues = stats.queues
keys = queues.keys

total = 0

keys.each do |key|
	total += queues[key]
end

puts "<<<local>>>"

status = 0
if total >= CRITICAL 
	status = 2
elsif total >= WARNING
	status = 1
end

print "#{status} SIDEKIQ_QUEUES total=#{total};#{WARNING};#{CRITICAL};0;"
keys.each do |key|
	print "|#{key}=#{queues[key]};;;0;"
end
puts " total #{total} jobs in queue"
'|grep -v sentry
