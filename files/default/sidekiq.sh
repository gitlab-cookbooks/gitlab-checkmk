#!/bin/bash
sidekiq_jobs=$(ps -ef|grep sidekiq|grep busy|awk '{ print $11 }'|sed -e 's/.//')

echo "0 Sidekiq jobs=$sidekiq_jobs;;;0; OK - $sidekiq_jobs jobs running"
